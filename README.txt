NEW VERSION

Requirements:

If not using the CSE machines then you will need to install graphviz.
The assignment must be in the same directory as m_test.pl and show_graph.sh.
Note that this was written to test a python3 assignment so if you didn't use
python3 you will need to modify the m_test.pl script.

Making tests:

Same as old version except script is called m_test.pl.
So execute like so:
perl m_tests.pl <number of nodes> <name of folder>
Note that the folder must not already exist.

Running the tests:

Similar to old version but instead there is a select menu instead.
So change directories to the test folder and run the script i.e.
sh run.sh
Several terminals will open each corresponding to a router. The
bottom right terminal will be the control terminal that allows
you to view the network topology, find the shortest paths and kill
routers.
_______________________________________________________________________________

OLD VERSION but has a better GUI

Requirements:

In order to use these files, you need to have the python igraph module.
On the CSE machines, igraph is not installed. To install on the CSE machines
download the source file from http://igraph.org/python/ and locally install it.
E.g. python setup.py --user

Also it assumes that your assignment is in the same directory as these scripts.
Note that the scripts will have to be modified if assignment is not written with
python3.


Making tests:

To generate the config files and the run script use the make_test.pl script.
The program generates a random connected graph and config files with currently
free port numbers.

It takes two command line arguments. The first is the number of routers in the
network. The second is the name of the folder to put the tests and run script in.
Note that the folder must not already exist in the current directory.
E.g. perl make_test.pl 6 test01

Running the tests:

Assuming that the run script is run soon after the script is generated change the
directory to the one where the config files and run script are. Then run the script
i.e. sh run.sh

Commands within the run script:

Once the run script is run, it opens up the a terminal for each router and executes
it. It also opens a terminal for a graph tester program. The graph tester program
acts like an interactive terminal. The commands for the program are as follows:
    plot - Shows the current network topology.
    shortest <Router ID> - Finds all the shortest paths from the given router.
    paths <route|plot|both> - Shows the paths found using the shortest command.
        If the second argument is 'route', then it prints out the least cost
        paths from the router with cost. addotopm 
        If the second argument is 'plot', it shows the  shortest paths graphically.
        If the seconde argument it plots are prints the shortest paths.
    delete <Router ID> - Removes router from network topology and kills that router.

Extra Notes:

    If assignment was written in a different language, then the make_tests.pl
    scritpt will need to be modified.
